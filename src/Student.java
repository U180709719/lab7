public class Student {
    double gradeFirst;
    double gradeSecond;
    double gradeThird;

    public Student(double gradeFirst, double gradeSecond, double gradeThird) {
        this.gradeFirst = gradeFirst;
        this.gradeSecond = gradeSecond;
        this.gradeThird = gradeThird;
    }
    //Here in the code, there are two types of sorting done - one is in place while the other is outplace sorting,
    //  now the basic difference between them is in place sorting doesn't take any extra space
    //  while on the other hand outplace sorting takes extra space

    public Student(Student s) {
        this(s.gradeFirst, s.gradeSecond, s.gradeThird);
    }

    public void inPlaceSort() {  //When we see the in-place sorting method:
        helperSort(this);     // The argument passed in the helperSort function is 'this' which refers
                                // to the current array of marks referred to as student (which is an object)
                                // of Student class. And what helperSort is doing is sorting the same array.
                                // While on the other hand, when we look at the outplace sort method:
    }

    public Student outPlaceSort() {            //A new Student object has been formed named aux
        Student aux = new Student(this);    // and it contains the same elements as the original array,
        helperSort(aux);                       // but now the new array is passed as an argument to the helperSort() method,
        return aux;                            // and the sorted array aux is returned in the outPlaceSort() method,
        // which results in extra memory.
    }

    @Override
    public String toString() {
        return this.gradeFirst + " " + this.gradeSecond + " " + this.gradeThird;
    }

    private void helperSort(Student s) {
        double[] fieldArray = new double[]{s.gradeFirst, s.gradeSecond, s.gradeThird};
        double temp;
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = i + 1; j < fieldArray.length ; j++) {
                if (fieldArray[j] < fieldArray[i]) {
                    temp = fieldArray[i];
                    fieldArray[i] = fieldArray[j];
                    fieldArray[j] = temp;
                }
            }
        }
        s.gradeFirst = fieldArray[0];
        s.gradeSecond = fieldArray[1];
        s.gradeThird = fieldArray[2];

    }


}
